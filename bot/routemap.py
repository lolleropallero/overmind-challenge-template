
from PIL import Image
import numpy as np

from pathfinding.core.diagonal_movement import DiagonalMovement
from pathfinding.core.grid import Grid
from pathfinding.finder.a_star import AStarFinder

im = Image.open("pathing_grid.png")
# data is in blue component (RGB)
matrix = np.array(im)[:,:,2]
# pixel is 0 if it is in path
# in pathfinding 0 is not in path
# we convert 0 --> True, non-zero --> False
matrix = matrix == 0
# convert True --> 1, False --> zero
matrix = matrix.astype(int)
grid = Grid(matrix=matrix)

start = grid.node(70,30)
end = grid.node(140,120)

finder = AStarFinder(diagonal_movement=DiagonalMovement.always)
path, runs = finder.find_path(start, end, grid)

route_map = matrix.copy()

#mark the path with number 8
for x,y in path:
     route_map[y,x] = 8

for r in range(route_map.shape[0]):
     for c in range(route_map.shape[1]):
         print(route_map[r,c] , end="")
     print("")

# print playable area from Abyssal Reef
# turn the picure & map coordinates, looks ok,
# but can be so just by coincidence
# (24, 4, 152, 136)
route_map = route_map.T
print("route_map.shape", route_map.shape)
y1 = 24 + 152
y0 = 24
x1 = 4 + 136 + 1
x0 =  4
for r in range(y1, y0,-1):
     for c in range(x0, x1):
         print(route_map[r,c] , end="")
     print("")

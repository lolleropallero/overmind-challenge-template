import json, random
from pathlib import Path

from .pathfinding.finder import a_star

import sc2
from sc2 import Race, Difficulty
from sc2.constants import *
from sc2.player import Bot, Computer
from sc2.position import Point2, Point3
from sc2.unit import Unit
from sc2.ids.unit_typeid import UnitTypeId
from sc2.ids.ability_id import AbilityId

# Bots are created as classes and they need to have on_step method defined.
# Do not change the name of the class!
class MyBot(sc2.BotAI):
    with open(Path(__file__).parent / "../botinfo.json") as f:
        NAME = json.load(f)["name"]
        MINERALS = 0
        GAS = 0
        MAIN_BASE = None
        MAIN_RAMP = None
        HOME_DEFENSE = []
        AVAILABLE_WORKERS = []
        HATCHERY_QUEENS = {}
        ATTACK_ARMY = []
        mboost_started = False
        HATCHERY_WAYPOINT_SET = []
        
    def select_target(self, combat_unit, defending=True):
        enemies = self.known_enemy_units
        can_attack_ground = combat_unit.can_attack_ground
        can_attack_air = combat_unit.can_attack_air
        if combat_unit.is_flying and not can_attack_ground:
            enemies = enemies.filter(lambda u: u.is_flying or u.is_massive)
        if not combat_unit.is_flying and not can_attack_air:
            enemies = enemies.filter(lambda u: not u.is_flying)
        if enemies.exists:
            enemies_in_range = enemies.filter(lambda u: combat_unit.target_in_range(u))
            if enemies_in_range:
                lowest_hp_enemy = min(enemies_in_range, key=lambda u: u.health)
                # TODO: Fix not targeting eggs
                if not lowest_hp_enemy.name == 'EGG':
                    return lowest_hp_enemy
            else:
                closest_ally_building = self.units.structure.closest_distance_to(combat_unit)
                if defending and closest_ally_building > 12:
                    return None
                closest_enemy = enemies.closest_to(combat_unit)
                if not defending and closest_enemy.distance_to(combat_unit) > 30:
                    # Lets ignore enemies if they are not too close
                    # and go for enemy structure destruction
                    pass
                elif not closest_enemy.name == 'EGG':
                    return closest_enemy
        elif not defending:
            target = self.known_enemy_structures.random_or(random.choice(self.enemy_start_locations)).position
            return target
        return None

    def get_unittype_in_eggs(self, name):
        total = 0
        for e in self.units(EGG):
            if e.orders[0].ability.button_name == name:
                total += 1
        return total

    def set_hatcheries_waypoint(self, waypoint, actions):
        hatcheries = self.townhalls.filter(lambda h: h.tag not in self.HATCHERY_WAYPOINT_SET)
        for hatchery in hatcheries:
            actions.append(hatchery(AbilityId.RALLY_UNITS, waypoint))
            self.HATCHERY_WAYPOINT_SET.append(hatchery.tag)
        return actions
    # On_step method is invoked each game-tick and should not take more than
    # 2 seconds to run, otherwise the bot will timeout and cannot receive new
    # orders.
    # It is important to note that on_step is asynchronous - meaning practices
    # for asynchronous programming should be followed.
    async def on_step(self, iteration):
        actions = []
        self.MINERALS = self.minerals
        self.GAS = self.vespene
        if hasattr(self, "time_budget_available"):
            if self.time_budget_available < 0.2:
                return
        if iteration == 0:
            await self.chat_send(f"Name: {self.NAME}")
            # Setup some initial values
            self.MAIN_BASE = self.units(HATCHERY).first
            self.MAIN_RAMP = self.main_base_ramp.upper.pop()
        if not self.townhalls.exists:
            # TODO: Fight to the bitter end
            await self.chat_send("Blin")
            await self._client.leave()
            return
        try:
            # This returns none if all hatcheries are doing something
            hatchery = self.townhalls.ready.random
        except:
            hatchery = self.townhalls.first
        actions = self.set_hatcheries_waypoint(self.MAIN_RAMP, actions)
        # Check that all hatcheries are in HATCHERY_QUEENS:
        for h in self.townhalls:
            if h.tag not in self.HATCHERY_QUEENS:
                self.HATCHERY_QUEENS[h.tag] = None
        # Remove destroyed hatcheries from HATCHERY_QUEENS:
        removable_tags = []
        for tag in self.HATCHERY_QUEENS:
            h = self.units.find_by_tag(tag)
            if not h:
                removable_tags.append(tag)
        for tag in removable_tags:
            self.HATCHERY_QUEENS.pop(tag)
        # Map unmapped queens to hatcheries:
        for q in self.units(QUEEN):
            if not q.tag in self.HATCHERY_QUEENS.values():
                for h_tag in self.HATCHERY_QUEENS:
                    if self.HATCHERY_QUEENS[h_tag] == None:
                        self.HATCHERY_QUEENS[h_tag] = q.tag
        # Remove dead queens from HATCHERY_QUEENS:
        for h_tag in self.HATCHERY_QUEENS:
            q = self.units.find_by_tag(self.HATCHERY_QUEENS[h_tag])
            if not q:
                self.HATCHERY_QUEENS[h_tag] = None
        # Get all larvas
        larvae = self.units(LARVA)
        # Send drones to work
        await self.distribute_workers()
        
        # Spawn moar larvae
        for queen in self.units(QUEEN).idle:
            abilities = await self.get_available_abilities(queen)
            if AbilityId.EFFECT_INJECTLARVA in abilities:
                for h_tag in self.HATCHERY_QUEENS:
                    print(h_tag)
                    print(self.HATCHERY_QUEENS)
                    print(queen.tag)
                    if queen.tag == self.HATCHERY_QUEENS[h_tag]:
                        h = self.units.find_by_tag(h_tag)
                        if h:
                            actions.append(queen(EFFECT_INJECTLARVA, h))
                
        if self.vespene >= 100:
            sp = self.units(SPAWNINGPOOL).ready
            if sp.exists and self.minerals >= 100 and not self.mboost_started:
                await self.do(sp.first(RESEARCH_ZERGLINGMETABOLICBOOST))
                self.mboost_started = True

        # Empty home defense if no enemies
        if not self.known_enemy_units:
            self.HOME_DEFENSE = []

        drones = self.workers
        for d in drones:
            if d.tag not in self.HOME_DEFENSE:
                if not d.orders or len(d.orders) == 1 and d.orders[0].ability.id in {AbilityId.MOVE, AbilityId.HARVEST_GATHER, AbilityId.HARVEST_RETURN}:
                    self.AVAILABLE_WORKERS.append(d.tag)
                
        # Assign attackers
        army = self.units.not_structure.exclude_type([
            UnitTypeId.LARVA, UnitTypeId.EGG, UnitTypeId.DRONE,
            UnitTypeId.QUEEN, UnitTypeId.OVERLORD
        ])
        for a in army:
            if a.tag not in self.HOME_DEFENSE:
                self.HOME_DEFENSE.append(a.tag)

        
        if army.amount > 20 and self.ATTACK_ARMY == []:
            self.ATTACK_ARMY = self.HOME_DEFENSE[:]
            self.HOME_DEFENSE = []
            
        army = self.units.filter(lambda a: a.tag in self.ATTACK_ARMY)
        
        if self.known_enemy_units:
            for a in army.idle:
                target = self.known_enemy_units.random
                if a.distance_to(target) < 20:
                    actions.append(a.attack(self.known_enemy_units.random))
        else:
            target = self.known_enemy_structures.random_or(self.enemy_start_locations[0]).position
            for a in army.idle:
                actions.append(a.attack(target))
        if army.amount < 10:
            for a in army:
                if a.distance_to(self.MAIN_RAMP) > 25:
                    actions.append(a.move(self.MAIN_RAMP))
            self.ATTACK_ARMY = []
                    
        # Assign drones to kill attackers early in the game (worker rush)
        # TODO: Assign drones in other situations as well
        # TODO: Create new file for actions
        if self.time < 120 and self.known_enemy_units:
            self.HOME_DEFENSE = self.AVAILABLE_WORKERS[:]
            self.AVAILABLE_WORKERS = []
            
        # Defend against the rush
        for tag in self.HOME_DEFENSE:
            unit = self.units.find_by_tag(tag)
            if not unit:
                self.HOME_DEFENSE.remove(tag)
            elif self.known_enemy_units:
                if self.time < 120:
                    actions.append(unit.attack(self.MAIN_RAMP))
                else:
                    target = self.select_target(unit)
                    if target:
                        actions.append(unit.attack(target))
        # If no enemies move to the main ramp
        if not self.known_enemy_units:
            army = self.units.filter(lambda a: a.tag in self.HOME_DEFENSE)
            for a in army.idle:
                if a.distance_to(self.MAIN_RAMP) > 5:
                    await self.do(a.move(self.MAIN_RAMP))
            
        # Build more overlords when nearing supply limit
        print('BUILD OVERLORDS')
        if self.supply_left < 2 and self.supply_cap < 200:
            if self.supply_cap < 30:
                if not self.already_pending(OVERLORD):
                    if self.can_afford(OVERLORD) and larvae.exists:
                        await self.do(larvae.random.train(OVERLORD))
            else:
                overlords_hatching = self.get_unittype_in_eggs('Overlord')
                if overlords_hatching < 2:
                    if self.can_afford(OVERLORD) and larvae.exists:
                        await self.do(larvae.random.train(OVERLORD))
                
        # Build drones
        print('BUILD DRONES')
        if self.workers.amount < (self.townhalls.amount*16 + self.units(EXTRACTOR).amount*3) and self.workers.amount < 60:
            drones_hatching = self.get_unittype_in_eggs('Drone')
            if self.can_afford(DRONE) and larvae.exists and drones_hatching < 3:
                await self.do(larvae.random.train(DRONE))
                
        # Build KEKEKEKEKEKEKE
        print('BUILD ZERGLINGS')
        if self.units(SPAWNINGPOOL).ready.exists:
            if larvae.exists and self.can_afford(ZERGLING) and self.supply_left > 0:
                await self.do(larvae.random.train(ZERGLING))
        
        # Build queens
        print('BUILD QUEEN')
        if self.units(QUEEN).amount < self.townhalls.ready.amount and self.units(SPAWNINGPOOL).ready.exists:
            if self.can_afford(QUEEN) and not self.already_pending(QUEEN) and hatchery.noqueue:
                await self.do(hatchery.train(QUEEN))
                
        # Build spawnign pool
        print('BUILD SPWAN')
        if not self.units(SPAWNINGPOOL).exists:
            if self.can_afford(SPAWNINGPOOL):
                # TODO: Smarter build algorithm
                for d in range(4, 15):
                    pos = hatchery.position.to2.towards(self.game_info.map_center, d)
                    if await self.can_place(SPAWNINGPOOL, pos):
                        ws = self.workers.filter(lambda u: u.tag in self.AVAILABLE_WORKERS)
                        w = ws.closest_to(pos)
                        self.AVAILABLE_WORKERS.remove(w.tag)
                        actions.append(w.build(SPAWNINGPOOL, pos))
                        self.MINERALS -= 150
        # Build gas
        print('BUILD GAS')
        if self.units(SPAWNINGPOOL).amount > 0:
            if self.units(EXTRACTOR).amount < self.townhalls.amount*2 and not self.already_pending(EXTRACTOR):
                if self.can_afford(EXTRACTOR) and ((self.units(DRONE).amount / max(self.units(EXTRACTOR).amount, 1) > 16)):
                    vgs = self.state.vespene_geyser.closer_than(20.0, hatchery)
                    for vg in vgs:
                        if self.units(EXTRACTOR).closer_than(1.0, vg).exists:
                            break
                        worker = self.select_build_worker(vg.position)
                        if worker is None:
                            break
                        actions.append(worker.build(EXTRACTOR, vg))
                        self.AVAILABLE_WORKERS.remove(worker.tag)
                        self.MINERALS -= 25
                        break
                            
        if self.units(SPAWNINGPOOL).amount > 0 and not self.already_pending(HATCHERY) and self.can_afford(HATCHERY) and self.minerals > 350:
            print('EXPAND')
            await self.expand_now()
        await self.do_actions(actions)
        
